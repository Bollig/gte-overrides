# GTE Overrides

## Getting started

* Install a Chrome addon that allows you to add custom CSS to any website (<https://chrome.google.com/webstore/search/custom%20css?hl=en>)
* Copy and paste the [CSS from this repo](gte-css-overrides.css) and ensure it's applied to the GTE domain.
* Delete sections if you don't want the changes—stick to removing CSS in blocks between comments if you're not sure what you're doing.

## Notes

This is tested only on desktop and across time entry only. If you have access to another part of GTE I don't then these likely won't help.
